drop view if exists AllEmployeesView;
drop table if exists Records;
drop table if exists GivenEquipment;
drop table if exists Equipment;
drop table if exists Employees;
drop table if exists Permissions;
drop table if exists Positions;