/* Get user */
select
	worker.Id,
	worker.NickName,
	worker.FirstName,
	worker.SecondName,
	worker.HomeAddress,
	worker.Phone,
	worker.DateHired,
	worker.DateFired,
	worker.Position,
	worker.Permission

from AllEmployeesView worker

where
	worker.Id = ?;


/* Get users by their boss */
select
	worker.Id,
	worker.NickName,
	worker.FirstName,
	worker.SecondName,
	worker.HomeAddress,
	worker.Phone,
	worker.DateHired,
	worker.DateFired,
	worker.Position,
	worker.Permission

from AllEmployeesView worker

where
	worker.BossId = ?;


/* Get users and employees by their boss */
select
	worker.Id,
	worker.NickName,
	worker.FirstName,
	worker.SecondName,
	worker.HomeAddress,
	worker.Phone,
	worker.DateHired,
	worker.DateFired,
	worker.Position,
	worker.Permission,

	boss.Id,
	boss.NickName,
	boss.FirstName,
	boss.SecondName,
	boss.HomeAddress,
	boss.Phone,
	boss.DateHired,
	boss.DateFired,
	boss.Position,
	boss.Permission

from AllEmployeesView worker

	inner join AllEmployeesView boss
	on worker.BossId = boss.Id


/* Get all equipment */
select
	Id,
	Model,
	Count

from Equipment;


/* Get all not returned equipment */
select
	Id,
	Model,
	count(Id) as CountGiven

from GivenEquipment

where
	EmployeeId = ? and DatetimeReturned is null

group by Id, Model