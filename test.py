from api import storage


def main():
    connector = storage("database.db")
    connector.drop_tables()
    connector.create_tables()
    connector.print_tables()
    print("----")
    connector.print_views()


if __name__ == "__main__":
    main()