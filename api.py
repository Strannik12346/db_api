import sqlite3


DB_PATH = "database.db"


class storage:
    @classmethod
    def get_storage(cls):
        if cls.storage is not None:
            cls.storage = storage(DB_PATH)

        return cls.storage


    def __init__(self, filepath):
        self.path = filepath
        self.drop_tables()
        self.create_tables()


    def _cursor(self):
        conn = sqlite3.connect(self.path)
        cursor = conn.cursor()
        return cursor


    def _commit_transaction(self, query, args):
        conn = sqlite3.connect(self.path)
        cursor = conn.cursor()
        cursor.execute(query, args)
        conn.commit()


    def _get_one(self, query, args):
        conn = sqlite3.connect(self.path)
        cursor = conn.cursor()
        return cursor.execute(query, args).fetchone()


    def _get_all(self, query, args):
        conn = sqlite3.connect(self.path)
        cursor = conn.cursor()
        return cursor.execute(query, args).fetchall()


    def create_tables(self):
        cursor = self._cursor()

        with open('init_scripts/init_positions.sql') as query:
            cursor.execute(query.read())

        with open('init_scripts/init_permissions.sql') as query:
            cursor.execute(query.read())

        with open('init_scripts/init_employees.sql') as query:
            cursor.execute(query.read())

        with open('init_scripts/init_equipment.sql') as query:
            cursor.execute(query.read())

        with open('init_scripts/init_given_equipment.sql') as query:
            cursor.execute(query.read())

        with open('init_scripts/init_records.sql') as query:
            cursor.execute(query.read())

        with open('init_scripts/init_view_all_employees.sql') as query:
            cursor.execute(query.read())


    def drop_tables(self):
        cursor = self._cursor()

        with open('drop_script.sql', 'r') as drop_script:
            cursor.executescript(drop_script.read())


    def print_tables(self):
        cursor = self._cursor()

        table_names = cursor.execute("select * from sqlite_master where type = 'table'").fetchall()
        for table_name in table_names:
            print(table_name)


    def print_views(self):
        cursor = self._cursor()

        view_names = cursor.execute("select * from sqlite_master where type = 'view'").fetchall()
        for view_name in view_names:
            print(view_name)


    def HireEmployee(self, employee):
        query = """
        
        insert into Employees 
              (NickName, FirstName, SecondName, HomeAddress, Phone, DateHired, DateFired, PositionId, PermissionId, BossId)
        values(       ?,         ?,          ?,           ?,     ?,     now(),      null,          ?,            ?,      ?);
        
        """

        args = [
            employee.username,
            employee.first_name,
            employee.second_name,
            employee.home_address,
            employee.phonenumber,
            employee.position,
            employee.permissions,
            employee.boss_id,
        ]

        self._commit_transaction(query, args)

    def MoveEmployee(self, employee):
        query = """
        
        update Employees
        set 
            PositionId   = ?,
            PermissionId = ?,
            BossId       = ?
        where
            Id = ?;
            
        """

        args = [
            employee.position,
            employee.permissions,
            employee.boss_id,
            employee.id
        ]

        self._commit_transaction(query, args)


    def FireEmployee(self, user_id):
        query = """
        
        update Employees
        set
            DateFired = now(),
        where
            Id = ?;
            
        """

        args = [
            user_id
        ]

        self._commit_transaction(query, args)


    def AddNewEquipmentToStorage(self, equipment):
        query = """
        
        insert into Equipment 
	          (Model, Count)
        values(    ?,     ?);  
        
        """

        args = [
            equipment.model,
            equipment.count
        ]

        self._commit_transaction(query, args)


    def AddExistingEquipmentToStorage(self, equipment_id, count_added):
        query = """

                update Equipment
                set
                    Count = Count + ?
                where
                    Id = ?;

                """

        args = [
            count_added,
            equipment_id
        ]

        self._commit_transaction(query, args)


    def RemoveEquipmentFromStorage(self, equipment_id, count_reduced):
        query = """

                update Equipment
                set
                    Count = Count - ?
                where
                    Id = ?;

                """

        args = [
            count_reduced,
            equipment_id
        ]

        self._commit_transaction(query, args)


    def GiveEquipment(self, equipment_id, employee_id, reason):
        query = """
        
        insert into GivenEquipment 
              ( DatetimeGiven, DatetimeReturned, ReasonGiven, EquipmentId, EmployeeId)
        values(         now(),             null,           ?,           ?,          ?);
        
        """

        args = [
            reason,
            equipment_id,
            employee_id
        ]

        self._commit_transaction(query, args)


    def ReturnEquipment(self, equip_id):
        query = """
        
        update GivenEquipment
        set
            DatetimeReturned = now()
        where
            Id = ?;
        
        """

        args = [
            equip_id
        ]

        self._commit_transaction(query, args)


    def GetUserProfile(self, user_id):
        query = """
        
        select 
            worker.Id, 
            worker.NickName, 
            worker.FirstName, 
            worker.SecondName, 
            worker.HomeAddress, 
            worker.Phone, 
            worker.DateHired, 
            worker.DateFired,
            worker.Position,
            worker.Permission,
            worker.BossId
        
        from AllEmployeesView worker
        
        where 
            worker.Id = ?;
        
        """

        args = [
            user_id
        ]

        return self._get_one(query, args)


    def GetUserByName(self, username):
        query = """
        
        select 
            worker.Id, 
            worker.NickName, 
            worker.FirstName, 
            worker.SecondName, 
            worker.HomeAddress, 
            worker.Phone, 
            worker.DateHired, 
            worker.DateFired,
            worker.Position,
            worker.Permission,
            worker.BossId
        
        from AllEmployeesView worker
        
        where 
            worker.NickName = ?;
        
        """

        args = [
            username
        ]

        return self._get_one(query, args)


    def GetUserHistory(self, user_id):
        history = [('blah', 'gdgfsgs')]

        return history


    def GetEquipmentById(self, equip_id):
        query = """
        
        select
            Id,
            Model,
            Count
            
        from Equipment
        
        where Id = ?
        
        """

        args = [
            equip_id
        ]

        return self._get_one(query, args)


    def GetAllEquipment(self):
        query = """
        
        select
            Id,
            Model,
            Count
        
        from Equipment;

        
        """

        return self._get_all(query, [])


    def GetOwnedEquipment(self, user_id):
        query = """
        
        select
            Id,
            Model,
            count(Id) as CountGiven
            
        from GivenEquipment
            
        where 
            EmployeeId = ? and DatetimeReturned is null
            
        group by Id, Model
        
        """

        args = [
            user_id
        ]

        return self._get_all(query, args)


    def GetAllEquipmentWithCountRest(self):
        query = """
        
        select
            eq.Id                      as Id,
            eq.Model                   as Model,
            eq.Count                   as Count,
            eq.Count - count(given.Id) as CountRest
            
        from Equipment eq
        
            inner join GivenEquipment given
            on eq.Id = given.EquipmentId
            
        where
            DatetimeReturned is null
            
        group by eq.Id, eq.Model, eq.Count
        
        """

        return self._get_all(query, [])


    def GetUsers(self, boss_id):
        query = """
        
        select 
            worker.Id, 
            worker.NickName, 
            worker.FirstName, 
            worker.SecondName, 
            worker.HomeAddress, 
            worker.Phone, 
            worker.DateHired, 
            worker.DateFired,
            worker.Position,
            worker.Permission
        
        from AllEmployeesView worker
        
        where 
            worker.BossId = ?;
        
        """

        args = [
            boss_id
        ]

        return self._get_all(query, args)
