from enum import Enum


class UserEquipment:
    def __init__(self, id, name, reason, date_given):
        self.id = id,
        self.name = name,
        self.reason = reason,
        self.date_given = date_given,


class Equipment:
    def __init__(self, id, model, count, count_free):
        self.id = id,
        self.model = model,
        self.count = count,
        self.count_free = count_free


class User:
    def __init__(self,
        id=None,
        username=None,
        first_name=None,
        second_name=None,
        home_address=None,
        phonenumber=None,
        date_hired=None,
        date_fired=None,
        position=None,
        permissions=None,
        boss_username=None
    ):

        self.id = id
        self.username = username
        self.first_name = first_name
        self.second_name = second_name
        self.home_address = home_address
        self.phonenumber = phonenumber
        self.date_hired = date_hired
        self.date_fired = date_fired
        self.position = position
        self.permissions = permissions if permissions is not None else Permissions.WORKER
        self.boss_username = boss_username


class Permissions(Enum):
    ADMIN  = 1
    WORKER = 2
    HR     = 3
    EQUIPS = 4