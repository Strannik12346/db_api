from functools import partial

from api import storage
from entities import User
from entities import Equipment
from entities import UserEquipment
from entities import Permissions


def GetUserProfile(user_id):
    db = storage.get_storage()
    row = db.GetUserProfile(user_id)
    (id, nickname, first, second, address, phone, date_hired, date_fired, position, permissions, boss_id) = row

    boss_row = db.GetUserProfile(boss_id)

    return User(
        id,
        nickname,
        first,
        second,
        address,
        phone,
        date_hired,
        date_fired,
        position,
        permissions,
        row[1]
    )


def GetUserHistory(user_id):
    pass


def UpdateUser(user):
    pass


def _row_to_user(row, boss_username):
    (id, nickname, first, second, address, phone, date_hired, date_fired, position, permissions, boss_id) = row

    return User(
        id,
        nickname,
        first,
        second,
        address,
        phone,
        date_hired,
        date_fired,
        position,
        permissions,
        boss_username
    )


def GetUsers(boss_id):
    db = storage.get_storage()
    boss = GetUserProfile(boss_id)

    raw_list = db.GetUsers(boss_id)
    func = partial(_row_to_user, boss_username = boss.username)
    users_list = map(func, raw_list)

    return users_list


def GetUserProfileByUsername(username):
    db = storage.get_storage()
    row = db.GetUserByName(username)

    (id, nickname, first, second, address, phone, date_hired, date_fired, position, permissions, boss_id) = row

    return User(
        id,
        nickname,
        first,
        second,
        address,
        phone,
        date_hired,
        date_fired,
        position,
        Permissions(permissions),
        GetUserProfile(boss_id).username
    )


def _row_to_equipment(row):
    (id, model, count, count_rest) = row

    return Equipment(id, model, count, count_rest)


def GetAllEquipment():
    db = storage.get_storage()
    raw_list = db.GetAllEquipmentWithCountRest()

    func = _row_to_equipment
    equipment_list = map(func, raw_list)

    return equipment_list


def GetEquipment(equip_id):
    db = storage.get_storage()
    row = db.GetEquipmentById(equip_id)
    return _row_to_equipment(row)


def _row_to_user_equipment(row):
    (id, date_given, date_returned, reason, equip_id, employee_id) = row
    equipment = GetEquipment(equip_id)

    return UserEquipment(id, equipment.model, reason, date_given)


def GetOwnedEquipment(user_id):
    db = storage.get_storage()
    raw_list = db.GetOwnedEquipment(user_id)

    func = _row_to_user_equipment
    user_equipment_list = map(func, raw_list)

    return user_equipment_list


def GiveEquipment(user_id, equip_id, reason):
    db = storage.get_storage()
    db.GiveEquipment(equip_id, user_id, reason)


def ReturnEquipment(equip_id):
    db = storage.get_storage()
    db.ReturnEquipment(equip_id)


def CreateEquipment(equipment):
    db = storage.get_storage()
    db.AddNewEquipmentToStorage(equipment)


def RemoveEquipment(equip_id):
    db = storage.get_storage()
    db.RemoveEquipmentFromStorage(equip_id, 1)


def IncrementEquipment(equip_id):
    db = storage.get_storage()
    db.AddExistingEquipmentToStorage(equip_id, 1)
