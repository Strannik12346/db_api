create table Positions(
	Id       int          not null,
	Position varchar(100) not null,

	primary key(Id),
	unique(Position)
);