create table Permissions(
	Id         int          not null,
	Permission varchar(100) not null,

	primary key(Id),
	unique(Permission)
);