create table GivenEquipment(
	Id               int          not null,
	DatetimeGiven    datetime     not null,
	DatetimeReturned datetime         null,
	ReasonGiven      varchar(100) not null,
	EquipmentId      int          not null,
	EmployeeId       int          not null,

	primary key(Id),

	foreign key(EquipmentId) references Equipment(Id),
	foreign key(EmployeeId)  references Employees(Id)
);