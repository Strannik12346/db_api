create table Records(
	Id              int          not null,
	RecordContent   varchar(100) not null,
	DatetimeCreated datetime     not null,
	EmployeeId      int          not null,

	primary key(Id),

	foreign key(EmployeeId) references Employees(Id)
);