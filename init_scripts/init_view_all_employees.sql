create view AllEmployeesView as

select
	worker.Id              as Id,
	worker.NickName        as NickName,
	worker.FirstName       as FirstName,
	worker.SecondName      as SecondName,
	worker.HomeAddress     as HomeAddress,
	worker.Phone           as Phone,
	worker.DateHired       as DateHired,
	worker.DateFired       as DateFired,
	worker.BossId          as BossId

	pos.Position           as Position,
	perm.Permission        as Permission

from Employees worker

	inner join Positions pos
	on worker.PositionId = pos.Id

	inner join Permissions perm
	on worker.PermissionId = perm.Id;