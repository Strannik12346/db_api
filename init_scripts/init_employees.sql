create table Employees(
	Id           int          not null,
	NickName     varchar(100) not null,
	FirstName    varchar(100) not null,
	SecondName   varchar(100) not null,
	HomeAddress  varchar(100)     null,
	Phone        varchar(100)     null,
	DateHired    datetime     not null,
	DateFired    datetime         null,
	PositionId   int          not null,
	PermissionId int          not null,
	BossId       int              null,

	primary key(Id),
	unique(NickName),

	foreign key(PositionId)   references Positions(Id),
	foreign key(PermissionId) references Permissions(Id),
	foreign key(BossId)       references Employees(Id)
);