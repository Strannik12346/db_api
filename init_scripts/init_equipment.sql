create table Equipment(
	Id    int          not null,
	Model varchar(100) not null,
	Count varchar(100) not null,

	primary key(Id),
	unique(Model)
)